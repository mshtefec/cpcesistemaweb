<?php

namespace Sistema\CPCEBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * DatosType form.
 * @author Gonzalo Alonso <gonzaloalonsod@gmail.com>
 */
class DatosType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $afiNac1 = $options['data']->getAfiNac1();
        if (!is_null($afiNac1)) {
            if ($afiNac1->format('d/m/Y') == "30/11/-0001") {
                $afiNac1 = null;
            }
        }
            
        $afiNac2 = $options['data']->getAfiNac2();
        if (!is_null($afiNac2)) {
            if ($afiNac2->format('d/m/Y') == "30/11/-0001") {
                $afiNac2 = null;
            }
        }

        $afiNac3 = $options['data']->getAfiNac3();
        if (!is_null($afiNac3)) {
            if ($afiNac3->format('d/m/Y') == "30/11/-0001") {
                $afiNac3 = null;
            }
        }
        
        $afiNac4 = $options['data']->getAfiNac4();
        if (!is_null($afiNac4)) {
            if ($afiNac4->format('d/m/Y') == "30/11/-0001") {
                $afiNac4 = null;
            }
        }
        
        $afiNac5 = $options['data']->getAfiNac5();
        if (!is_null($afiNac5)) {
            if ($afiNac5->format('d/m/Y') == "30/11/-0001") {
                $afiNac5 = null;
            }
        }
        
        $afiNac6 = $options['data']->getAfiNac6();
        if (!is_null($afiNac6)) {
            if ($afiNac6->format('d/m/Y') == "30/11/-0001") {
                $afiNac6 = null;
            }
        }

        $builder
            /*
            ->add('afiNombre', null, array(
                'label'      => 'Apellido y Nombres',
            ))
            ->add('afiTipdoc', null, array(
                'label'      => 'Tipo',
            ))
            ->add('afiNrodoc', null, array(
                'label'      => 'Documento',
            ))
            ->add('afiCategoria', null, array(
                'label'      => 'Categoría',
            ))
            ->add('afiTitulo', null, array(
                'label'      => 'Título',
            ))
            ->add('afiMatricula', null, array(
                'label'      => 'Matrícula',
            ))
            ->add('afiZona', null, array(
                'label'      => 'Delegación',
            ))
            */
            ->add('afiDireccion', null, array(
                'label'      => 'Domicilio',
                'required'    => true
            ))
            ->add('afiLocalidad', null, array(
                'label'      => 'Localidad',
                'required'    => true
            ))
            ->add('afiProvincia', 'choice', array(
                'label'      => 'Provincia',  
                'choices' => array(
                    'Chaco'                => 'Chaco',
                    'Corrientes'           => 'Corrientes',
                    'Formosa'              => 'Formosa',
                    'Misiones'             => 'Misiones',
                    'Santa Fe'             => 'Santa Fe',
                    'Entre Rios'           => 'Entre Rios',
                    'Santiago del Estero'  => 'Santiago del Estero',
                    'Salta'                => 'Salta',
                    'Jujuy'                => 'Jujuy',
                    'Tucuman'              => 'Tucuman',
                    'Catamarca'            => 'Catamarca',
                    'La Rioja'             => 'La Rioja',
                    'San Juan'             => 'San Juan',
                    'Cordoba'              => 'Cordoba',
                    'Mendoza'              => 'Mendoza',
                    'Buenos Aires'         => 'Buenos Aires',
                    'La Pampa'             => 'La Pampa',
                    'Neuquen'              => 'Neuquen',
                    'Chubut'               => 'Chubut',
                    'Rio Negro'            => 'Rio Negro',
                    'Santa Cruz'           => 'Santa Cruz',
                    'Tierra del Fuego'     => 'Tierra del Fuego'
                )
            ))
            ->add('afiCodpos', null, array(
                'label'      => 'Código Postal',
                'required'    => true
            ))
            /*
            ->add('afiFecnac', 'bootstrapdatetime', array(
                'label'      => 'Fecha de Nacimiento',
                'widget_type' => 'date',
            ))
            ->add('afiSexo', null, array(
                'label'      => 'Sexo',
            ))
            ->add('afiCivil', null, array(
                'label'      => 'Estado Civil',
            ))
            */
            ->add('afiTelefono1', null, array(
                'label'      => 'Teléfono (10 caract.)',
                'attr'  => array(
                    'data-mask' => '(999)9999999',
                ),
                'required'    => true
            ))
            ->add('afiCelular', null, array(
                'label'      => 'Celular (Sin 0 ni 15)',
                'attr'  => array(
                    'data-mask' => '(999)9999999',
                ),
                'required'    => true
            ))
            ->add('afiMail', null, array(
                'label'      => 'Correo',
                'required'    => true
            )) 
            /*
            ->add('afiCuit', null, array(
                'label'      => 'CUIL',
            )) 
            ->add('afiTipo')
            */
            ->add('afiDoc1')
            ->add('afiAut1')
            ->add('afiNac1', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac1,
            ))
            ->add('afiSex1', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil1', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiDoc2')
            ->add('afiAut2')
            ->add('afiNac2', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac2,
            ))
            ->add('afiSex2', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil2', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiDoc3')
            ->add('afiAut3')
            ->add('afiNac3', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac3
            ))
            ->add('afiSex3', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil3', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiDoc4')
            ->add('afiAut4')
            ->add('afiNac4', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac4,
            ))
            ->add('afiSex4', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil4', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiDoc5')
            ->add('afiAut5')
            ->add('afiNac5', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac5,
            ))
            ->add('afiSex5', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil5', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiDoc6')
            ->add('afiAut6')
            ->add('afiNac6', 'bootstrapdatetime', array(
                'required'    => false,
                'widget_type' => 'date',
                'data'        => $afiNac6,
            ))
            ->add('afiSex6', 'choice', array(
                'choices' => array(
                    '-' => null,
                    'F' => 'Mujer',
                    'M' => 'Hombre'
                ),
                'choices_as_values' => false,
            ))
            ->add('afiFil6', 'choice', array(
                'choices' => array(
                    '-' => null,
                    '0' => 'Esposo/a',
                    '1' => 'Hijo/a',
                    '2' => 'Otro'
                ),
                'choices_as_values' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Sistema\CPCEBundle\Entity\Afiliado'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sistema_cpcebundle_afiliado';
    }
}
